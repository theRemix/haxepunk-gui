To work, this project needs HaXe+nme and haxepunk framework.

INSTALLATION
HaXe+nme :
haxelib install nme
or
http://www.haxenme.org/download/ and Use windows auto installer, then add the C:\Motion-Twin\Haxe folder to windows path.
If you're a linux user I asume you won't have any problem to install it with your favorite package manager or using instructions from http://www.haxenme.org/.

HaxePunk  :
haxelib install HaxePunk

DOCUMENTATION
HaxePunk  :
http://haxepunk.com/learn/
http://forum.haxepunk.com/
https://github.com/MattTuttle/HaxePunk
