package demo;
import com.haxepunk.gui.Button;
import com.haxepunk.gui.Control;
import com.haxepunk.gui.Label;
import com.haxepunk.gui.Panel;
import com.haxepunk.gui.TextInput;
import com.haxepunk.gui.ToggleButton;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import flash.events.Event;
import openfl.Assets;
import flash.geom.Point;
import flash.text.TextFormatAlign;

/**
 * ...
 * @author Lythom
 */

class ButtonDemoPanel extends DemoPanel
{
	
	private var buttons:Array<Button> ;
	private var testButton:Button;

	public function new(width:Int, height:Int)
	{
		super(0, 0, width, height, true);
		
		buttons = new Array<Button>();
		
		var cursor:Point = new Point(10, 10);
		var margin:Int = 10;
		
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		var l:Label = new Label("Button and ToggleButton", cursor.x, cursor.y, Math.round(width - cursor.x));
		l.align = TextFormatAlign.CENTER;
		l.size = 16;
		l.color = 0x000000;
		addControl(l);
		
		cursor.y += margin + l.height;
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		var b:Button = new Button("Auto-sized Button (0x0)", cursor.x, cursor.y, 0, 0);
		addControl(b);
		
		cursor.x += margin + b.width;
		var b:Button = new ToggleButton("Auto-sized Toggle Button (0x0)",false, cursor.x, cursor.y, 0, 0);
		addControl(b);
		
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		cursor.y += margin + b.height;
		b = new Button("fixed-height Button (0x50)", cursor.x, cursor.y, 0, 50);
		addControl(b);
		
		cursor.x += margin + b.width;
		var b:Button = new ToggleButton("fixed-height Toggled Button (0x50)",true, cursor.x, cursor.y, 0,50);
		addControl(b);
		
		cursor.x = globalInstructionLabel.localX + globalInstructionLabel.width + margin;
		cursor.y += margin + b.height;
		b = new Button("fixed-width left aligned Button (300x0)", cursor.x, cursor.y, 300, 0);
		b.label.align = TextFormatAlign.LEFT;
		addControl(b);
		
		cursor.y += margin + b.height;
		b = new Button("fixed-width centered Button (300x0)", cursor.x, cursor.y, 300, 0);
		addControl(b);
		
		cursor.y += margin + b.height;
		b = new Button("fixed-width right aligned Button (300x0)", cursor.x, cursor.y, 300, 0);
		b.label.align = TextFormatAlign.RIGHT;
		addControl(b);
		
		cursor.y += margin + b.height;
		ti = new TextInput("Testing text input", cursor.x, cursor.y, 300, 25, false, false);
		addControl(ti);
		
		cursor.y = globalInstructionLabel.y + globalInstructionLabel.height + margin;
		cursor.x = globalInstructionLabel.x;
		var instructions:String = "Specific Instructions :\n";
		instructions += "+ - increase text size\n";
		instructions += "- - decrease text size\n";
		instructions += "f - switch font family\n";
		instructions += "d - enable/disable buttons\n";
		l = new Label(instructions, cursor.x, cursor.y);
		addControl(l);
		
	}
	
	override private function set_enabled(value:Bool):Bool 
	{
		super.set_enabled(value);
		ti.enabled = value;
		return value;
	}
	
	override public function addControl(child:Control, ?position:Int):Void
	{
		if (Std.is(child, Button)) {
			buttons.push(cast(child,Button));
		}
		super.addControl(child, position);
	}
	
	var i = 0;
	var ti:TextInput;
	override public function update()
	{
		if (!enabled || ti.hasFocus) {
			return;
		}
		
		if (Input.pressed(Key.NUMPAD_ADD)) {
			for (b in buttons) {
				b.label.size++;
			}
		}
		if (Input.pressed(Key.NUMPAD_SUBTRACT)) {
			for (b in buttons) {
				b.label.size--;
			}
		}
		if (Input.pressed(Key.F)) {
			for (b in buttons) {
				if (b.font == Assets.getFont("font/pf_ronda_seven.ttf").fontName) {
					b.font = Assets.getFont("font/04B_03__.ttf").fontName;
				} else {
					b.font = Assets.getFont("font/pf_ronda_seven.ttf").fontName;
				}
			}
		}
		if (Input.pressed(Key.D)) {
			for (b in buttons) {
				b.enabled = !b.enabled;
			}
		}
		super.update();
	}
	
}